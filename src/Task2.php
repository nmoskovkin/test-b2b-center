<?php
namespace App;

class Task2
{
    public function urlManipulations(string $url): string
    {
        $urlData = parse_url($url);

        if (!isset($urlData['scheme'], $urlData['host'], $urlData['path'], $urlData['query'])) {
            throw new \InvalidArgumentException('Wrong url');
        }

        parse_str($urlData['query'], $queryParams);
        $queryParams = array_filter($queryParams, function ($val) {
            return $val != 3;
        });
        asort($queryParams);
        $queryParams['url'] = $urlData['path'];

        return $urlData['scheme'] . '://' . $urlData['host'] . '/?' . http_build_query($queryParams);
    }
}
