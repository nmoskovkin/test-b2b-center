<?php
namespace App\Task3\Repository;

use App\Task3\Entity\Article;
use App\Task3\Entity\User;

class ArticleRepository // extends someBaseRepository
{
    /**
     * @param Article $article
     */
    public function save(Article $article)
    {
        // $this->saveEntity($article);
    }

    /**
     * @param User $user
     *
     * @return Article[]
     */
    public function filterByUser(User $user)
    {

    }
}
