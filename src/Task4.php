<?php
namespace App;

use mysqli;

class Task4
{
    private $db;

    /**
     * @param mysqli $db
     */
    public function __construct(mysqli $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $userIdsString
     *
     * @return array
     */
    function getUserNames(string $userIdsString): array
    {
        $userIds = explode(',', $userIdsString);
        array_walk($userIds, function($val) {
           return (int)$val;
        });
        $userIdsString = implode(',', $userIds);
        $userIdsString = mysqli_escape_string($this->db, $userIdsString);
        $res = mysqli_query($this->db, sprintf('SELECT * FROM users WHERE id in (%s)', $userIdsString));

        $userNames = array();
        while ($user = $res->fetch_object()) {
            $userNames[$user->id] = $user->name;
        }

        return $userNames;
    }

    /**
     * Old implementation of getUserNames
     *
     * @deprecated
     *
     * @param $user_ids
     *
     * @return mixed
     */
    function load_users_data($user_ids) {
        $user_ids = explode(',', $user_ids);
        foreach ($user_ids as $user_id) {
            $db = mysqli_connect("10.10.0.2", "root", "root", "test");
            $sql = mysqli_query($db, "SELECT * FROM users WHERE id=$user_id");
            while($obj = $sql->fetch_object()){
                $data[$user_id] = $obj->name;
            }
            mysqli_close($db);
        }
        return $data;
    }
}
