<?php
namespace App\Tests;

use App\Task2;
use PHPUnit\Framework\TestCase;

class Task2Test extends TestCase
{
    public function testUrlManipulations()
    {
        $task = new Task2();

        $this->assertEquals(
            'https://www.somehost.com/?param4=1&param3=2&param1=4&url=%2Ftest%2Findex.html',
            $task->urlManipulations(
                'https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3'
            )
        );
    }
}
