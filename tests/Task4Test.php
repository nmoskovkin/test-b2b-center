<?php
namespace App\Tests;

use App\Task2;
use PHPUnit\Framework\TestCase;

class Task4Test extends TestCase
{
    /**
     * This is not a unit test, this is here just for run getUserNames!!!
     */
    public function testGetUserNames()
    {
        $db = mysqli_connect('10.10.0.2', 'root', 'root', 'test');
        $results = (new \App\Task4($db))->getUserNames('1,2,3');

        $this->assertEquals(count($results), 3);
    }
}
